package gitlab.quoopie.easyranks.tabCompleters;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class rankTabCompleter implements TabCompleter {

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

        ArrayList<String> subcommands = new ArrayList<>()
        {
            {
                add("help");
                add("list");
                add("give");
                add("take");
                add("add");
                add("remove");
                add("delete");
                add("setdefault");
            }
        };

        ArrayList<String> results = new ArrayList<>();

        results.clear();

        // arg0 Tab Completer
        if (args.length == 1) {

            for (String subcommand : subcommands) {
                if (subcommand.toLowerCase().startsWith(args[0].toLowerCase())) {
                    results.add(subcommand);
                }
            }

            return results;
            // help/list tab completer
        } else if (args.length == 2) {
            if (args[0].equals("help") || args[0].equals("list")) {
                return List.of();
            } else if (args[0].equals("give") || args[0].equals("take")) {
                ArrayList<String> usernames = new ArrayList<>();

                for (Player player : Bukkit.getOnlinePlayers()) {
                    usernames.add(player.getDisplayName());
                }

                for (String username : usernames) {
                    if (username.toLowerCase().startsWith(args[1].toLowerCase())) {
                        results.add(username);
                    }
                }
                return results;
            } else if (args[0].equals("add") || args[0].equals("remove")) {
                for (Permission permission : Bukkit.getPluginManager().getPermissions()) {
                    if (permission.getName().toLowerCase().startsWith(args[1].toLowerCase())) {
                        results.add(permission.getName());
                    }
                }
                return results;
            } else if (args[0].equals("delete") || args[0].equals("setdefault")) {
                try {
                    ArrayList<String> ranks = new ArrayList<>();
                    Scanner existingReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/existingRanks.txt")));
                    while (existingReader.hasNextLine()) {
                        ranks.add(existingReader.nextLine());
                    }
                    existingReader.close();

                    for (String rank : ranks) {
                        if (rank.startsWith(args[1])) {
                            results.add(rank);
                        }
                    }
                    return results;
                } catch (Exception e) {
                    e.printStackTrace();
                    return List.of();
                }
            }
        } else if (args.length == 3) {
            if (args[0].equals("help") || args[0].equals("list") || args[0].equals("delete") || args[0].equals("setdefault")) {
                return List.of();
            } else if (args[0].equals("give") || args[0].equals("add") || args[0].equals("remove")) {
                try {
                    ArrayList<String> ranks = new ArrayList<>();
                    Scanner existingReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/existingRanks.txt")));
                    while (existingReader.hasNextLine()) {
                        ranks.add(existingReader.nextLine());
                    }
                    existingReader.close();

                    for (String rank : ranks) {
                        if (rank.startsWith(args[2])) {
                            results.add(rank);
                        }
                    }
                    return results;
                } catch (Exception e) {
                    e.printStackTrace();
                    return List.of();
                }
            } else if (args[0].equals("take")) {
                String username = args[1];
                try {
                    Scanner rankReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/ranks.txt")));
                    while (rankReader.hasNextLine()) {
                        String line = rankReader.nextLine();
                        if (line.contains(username)) {
                            results.add(line.split(" ")[1]);
                        }
                    }
                    return results;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        return List.of();
    }
}
