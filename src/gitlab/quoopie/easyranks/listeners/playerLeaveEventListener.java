package gitlab.quoopie.easyranks.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class playerLeaveEventListener implements Listener {

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) {
        String username = event.getPlayer().getDisplayName();

        try {
            Scanner activeRanksReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/activeRanks.txt")));
            ArrayList<String> lines = new ArrayList<>();

            while (activeRanksReader.hasNextLine()) {
                lines.add(activeRanksReader.nextLine());
            }
            activeRanksReader.close();

            BufferedWriter activeRanksWriter = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/activeRanks.txt"));

            for (String line : lines) {
                if (!line.contains(username)) { activeRanksWriter.write(line); }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
