package gitlab.quoopie.easyranks.listeners;

import static gitlab.quoopie.easyranks.utilities.colorizer.colorize;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class playerJoinEventListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        boolean rankExists = false;

        try {
            Scanner rankReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/ranks.txt")));
            String username = event.getPlayer().getDisplayName();
            String rank = null;
            while (rankReader.hasNextLine()) {
                String line = rankReader.nextLine();
                if (line.contains(username)) {
                    rank = line.substring(username.length() + 1);
                    rankExists = true;
                }
            }

            rankReader.reset();

            if (!rankExists) {
                ArrayList<String> lines = new ArrayList<>();
                while (rankReader.hasNextLine()) {
                    lines.add(rankReader.nextLine());
                }
                rankReader.close();
                BufferedWriter rankWriter = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/ranks.txt"));
                for (String line : lines) {
                    rankWriter.write(line);
                }

                Scanner defaultReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/defaultRank.txt")));
                String defaultRank = defaultReader.nextLine();
                defaultReader.close();
                rankWriter.write(username + " "  + defaultRank);
                rankWriter.close();

                Scanner activeRanksReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/activeRanks.txt")));
                lines.clear();
                while (activeRanksReader.hasNextLine()) {
                    lines.add(activeRanksReader.nextLine());
                }
                activeRanksReader.close();
                BufferedWriter activeRanksWriter = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/activeRanks.txt"));
                for (String line : lines) {
                    activeRanksWriter.write(line);
                }
                activeRanksWriter.write(username + " " + defaultRank);
                activeRanksWriter.close();
                event.getPlayer().setPlayerListName(colorize(defaultRank) + " " + username);
            } else {
                Scanner activeRanksReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/activeRanks.txt")));
                ArrayList<String> lines = new ArrayList<>();

                while (activeRanksReader.hasNextLine()) {
                    lines.add(activeRanksReader.nextLine());
                }
                activeRanksReader.close();

                BufferedWriter activeRanksWriter = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/activeRanks.txt"));

                for (String line : lines) {
                    activeRanksWriter.write(line);
                }
                activeRanksWriter.write(username + " " + rank);
                activeRanksWriter.close();
                event.getPlayer().setPlayerListName(colorize(rank) + " " + username);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
