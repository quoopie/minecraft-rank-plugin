package gitlab.quoopie.easyranks.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import static gitlab.quoopie.easyranks.utilities.colorizer.colorize;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class chatEventListener implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        String message = event.getMessage();
        String username = event.getPlayer().getDisplayName();
        if (event.getPlayer().hasPermission("easyranks.coloredcchat")) {
            message = colorize(message);
        }

        event.setCancelled(true);

        String rank = fetchRank(username);

        Bukkit.broadcastMessage(colorize(rank) + " " + username + colorize("&r: ") + message);
    }


    public String fetchRank(String username) {
        try {
            Scanner rankReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/activeRanks.txt")));
            while (rankReader.hasNextLine()) {
                String line = rankReader.nextLine();
                if (line.contains(username)) {
                    rankReader.close();
                    return line.substring(username.length() + 1);
                }
            }
            rankReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new NullPointerException("Unreachable");
    }

}
