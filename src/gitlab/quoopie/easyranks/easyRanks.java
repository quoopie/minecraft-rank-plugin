package gitlab.quoopie.easyranks;

// Bukkit imports
import gitlab.quoopie.easyranks.commands.rankCommandExecutor;
import gitlab.quoopie.easyranks.listeners.playerJoinEventListener;
import gitlab.quoopie.easyranks.listeners.playerLeaveEventListener;
import gitlab.quoopie.easyranks.tabCompleters.rankTabCompleter;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

// Package imports
import gitlab.quoopie.easyranks.listeners.chatEventListener;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

public class easyRanks extends JavaPlugin {

    @Override
    public void onEnable() {

        initNeededFiles();

        getServer().getPluginManager().registerEvents(new chatEventListener(), this);
        getServer().getPluginManager().registerEvents(new playerJoinEventListener(), this);
        getServer().getPluginManager().registerEvents(new playerLeaveEventListener(), this);

        PluginCommand rankCommand = getCommand("rank");

        if (rankCommand != null) {
            rankCommand.setExecutor(new rankCommandExecutor());
            rankCommand.setTabCompleter(new rankTabCompleter());
        }
    }

    @Override
    public void onDisable() {
        clearActiveRanks();
    }

    public void clearActiveRanks() {
        try {
            BufferedWriter activeRanksClearer = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/activeRanks.txt"));
            activeRanksClearer.write("");
            activeRanksClearer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initNeededFiles() {
        ArrayList<File> files = new ArrayList<>() {
            {
                add(new File("./plugins/Easy Ranks/ranks.txt"));
                add(new File("./plugins/Easy Ranks/activeRanks.txt"));
                add(new File("./plugins/Easy Ranks/defaultRank.txt"));
                add(new File("./plugins/Easy Ranks/existingRanks.txt"));
            }
        };

        File parentDirectory = new File("./plugins/Easy Ranks");

        try {
            if (!parentDirectory.exists()) { boolean unused = parentDirectory.mkdirs(); }
            for (File file : files) {
                boolean fileCreated = file.createNewFile();
                if (fileCreated && file.getName().equals("defaultRank.txt")) {
                    BufferedWriter defaultMaker = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/defaultRank.txt"));
                    defaultMaker.write("&7[Default]");
                    defaultMaker.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
