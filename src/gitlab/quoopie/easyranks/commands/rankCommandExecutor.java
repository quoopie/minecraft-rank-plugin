package gitlab.quoopie.easyranks.commands;

import static gitlab.quoopie.easyranks.utilities.colorizer.colorize;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class rankCommandExecutor implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        CommandSender console = Bukkit.getConsoleSender();

        if (args.length > 0) {

            switch (args[0]) {
                case "help" -> //noinspection Redundent DuplicateBranchesInSwitch
                sendHelpMessages(sender);
                case "give" -> {
                    if (!sender.hasPermission("easyranks.giveranks")) {
                        sender.sendMessage(colorize("&cError! You do not have permission to execute this!"));
                        return true;
                    }

                    if (args.length != 3) {
                        sender.sendMessage(colorize("&c/rank give [username] [rank_name]"));
                        return true;
                    }

                    String username = args[1];
                    String rankName = args[2];

                    Bukkit.dispatchCommand(console, "lp user " + username + " addgroup " + rankName);

                    Player player = Bukkit.getPlayer(username);

                    if (player != null) {
                        player.setPlayerListName(colorize(rankName + " " + username));
                    }

                    boolean rankCreated = updateRank(username, rankName);
                    if (rankCreated) {
                        sender.sendMessage(colorize("&aCreated &enew rank: &r" + rankName));
                    }
                    sender.sendMessage(colorize("&aAdded &r" + rankName + " &eto &b" + username));
                }
                case "take" -> {
                    if (!sender.hasPermission("easyranks.removeranks")) {
                        sender.sendMessage(colorize("&cError! You do not have permission to execute this!"));
                        return true;
                    }

                    if (args.length != 3) {
                        sender.sendMessage(colorize("&c/rank take [username] [rank_name]"));
                        return true;
                    }

                    String username = args[1];
                    String rankName = args[2];

                    try {
                        Scanner ranksReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/ranks.txt")));
                        ArrayList<String> lines = new ArrayList<>();
                        while (ranksReader.hasNextLine()) {
                            lines.add(ranksReader.nextLine());
                        }
                        ranksReader.close();

                        Scanner defaultReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/defaultRank.txt")));
                        String defaultRank = defaultReader.nextLine();
                        defaultReader.close();

                        boolean playerExists = false;
                        for (String line : lines) {
                            if (line.contains(username)) {
                                playerExists = true;
                            }
                            if (line.contains(username) && !line.contains(rankName)) {
                                sender.sendMessage(colorize("&cError! That player does not have that rank"));
                                return true;
                            }
                        }

                        if (!playerExists) {
                            sender.sendMessage(colorize("&cError! That player has never joined the server!"));
                            return true;
                        }

                        @SuppressWarnings("unused")
                        boolean unused = updateRank(username, defaultRank);

                        Player player = Bukkit.getPlayer(username);

                        if (player != null) {
                            player.setPlayerListName(colorize(defaultRank + " " + username));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                case "add" -> {
                    if (!sender.hasPermission("easyranks.editranks")) {
                        sender.sendMessage(colorize("&cError! You do not have permission to execute this!"));
                        return true;
                    }

                    if (args.length != 3) {
                        sender.sendMessage(colorize("&c/rank add [permission] [rank_name]"));
                        return true;
                    }

                    String permission = args[1];
                    String rankName = args[2];

                    if (rankName.equals("default")) {
                        Bukkit.dispatchCommand(console, "lp default add " + permission);
                        return true;
                    } else {
                        Bukkit.dispatchCommand(console, "lp group " + rankName + " add " + permission);
                    }

                    try {
                        Scanner existingReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/existingRanks.txt")));
                        boolean rankExists = false;
                        ArrayList<String> lines = new ArrayList<>();
                        while (existingReader.hasNextLine()) {
                            String line = existingReader.nextLine();
                            if (line.contains(rankName)) {
                                rankExists = true;
                                break;
                            }
                            lines.add(line);
                        }
                        existingReader.close();

                        if (!rankExists) {
                            BufferedWriter existingWriter = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/existingRanks.txt"));
                            for (String line : lines) {
                                existingWriter.write(line);
                            }
                            existingWriter.write(rankName);
                            existingWriter.close();

                            sender.sendMessage(colorize("&aCreated &enew rank: " + rankName));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    sender.sendMessage(colorize("&eAdded permission &b" + permission + " &eto &r" + rankName));
                }
                case "remove" -> {
                    if (!sender.hasPermission("easyranks.editranks")) {
                        sender.sendMessage(colorize("&cError! You do not have permission to execute this!"));
                        return true;
                    }

                    if (args.length != 3) {
                        sender.sendMessage(colorize("&c/rank remove [permission] [rank_name]"));
                        return true;
                    }

                    String permission = args[1];
                    String rankName = args[2];

                    if (rankName.equals("default")) {
                        Bukkit.dispatchCommand(console, "lp default remove " + permission);
                    } else {
                        Bukkit.dispatchCommand(console, "lp group " + rankName + " remove " + rankName);
                    }
                    sender.sendMessage(colorize("&eRemoved permission &b" + permission + " &efrom &r" + rankName));
                }
                case "delete" -> {
                    if (!sender.hasPermission("easyranks.deleteranks")) {
                        sender.sendMessage(colorize("&cError! You do not have permission to execute this!"));
                        return true;
                    }

                    if (args.length != 2) {
                        sender.sendMessage(colorize("&c/rank delete [rank_name]"));
                        return true;
                    }

                    String rankName = args[1];
                    if (!checkExistingRanks(rankName)) {
                        sender.sendMessage(colorize("&cError! That rank does not exist!"));
                        return true;
                    }

                    try {
                        Scanner ranksReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/ranks.txt")));
                        Scanner defaultReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/defaultRank.txt")));
                        String defaultRank = defaultReader.next();
                        defaultReader.close();
                        while (ranksReader.hasNextLine()) {
                            String line = ranksReader.nextLine();
                            if (line.contains(rankName)) {
                                String username = line.split(" ")[0];

                                @SuppressWarnings("unused")
                                boolean unused = updateRank(username, defaultRank);

                                Player player = Bukkit.getPlayer(username);

                                if (player != null) {
                                    player.setPlayerListName(colorize(defaultRank + " " + username));
                                }
                            }
                        }
                        ranksReader.close();

                        Scanner existingReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/existingRanks.txt")));
                        ArrayList<String> lines = new ArrayList<>();
                        while (existingReader.hasNextLine()) {
                            lines.add(existingReader.nextLine());
                        }
                        existingReader.close();

                        BufferedWriter existingWriter = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/existingRanks.txt"));
                        for (String line : lines) {
                            if (!line.equals(rankName)) {
                                existingWriter.write(line);
                            }
                        }
                        existingWriter.close();

                        sender.sendMessage(colorize("&cDeleted &erank: &r" + rankName));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                case "list" -> {
                    if (!sender.hasPermission("easyranks.listranks")) {
                        sender.sendMessage(colorize("&cError! You do not have permission to execute this!"));
                        return true;
                    }

                    try {
                        Scanner existingReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/existingRanks.txt")));
                        sender.sendMessage(colorize("&eCurrent existing ranks:&r"));
                        while (existingReader.hasNextLine()) {
                            sender.sendMessage(colorize(existingReader.nextLine()));
                        }
                        existingReader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                case "setdefault" -> {
                    if (args.length != 2) {
                        sender.sendMessage("&c/rank setdefault [rank_name]");
                        return true;
                    }

                    try {
                        Scanner defaultReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/defaultRank.txt")));
                        String oldDefault = defaultReader.nextLine();
                        defaultReader.close();

                        String newDefault = args[1];

                        BufferedWriter defaultWriter = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/defaultRank.txt"));
                        defaultWriter.write(newDefault);
                        defaultWriter.close();

                        Scanner ranksReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/ranks.txt")));
                        while (ranksReader.hasNextLine()) {
                            String line = ranksReader.nextLine();
                            String username = line.split(" ")[0];
                            String rank = line.split(" ")[1];

                            if (rank.equals(oldDefault)) {
                                updateRank(username, newDefault);
                                Player player = Bukkit.getPlayer(username);

                                if (player != null) {
                                    player.setPlayerListName(colorize(newDefault + " " + username));
                                }
                            }
                        }
                        ranksReader.close();

                        sender.sendMessage(colorize("&eSet default rank to: &r" + newDefault));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                default -> sendHelpMessages(sender);
            }
        } else {
            sendHelpMessages(sender);
        }

        return true;
    }

    public void sendHelpMessages(CommandSender sender) {
        sender.sendMessage(colorize("&e/rank help -> Displays this message"));
        sender.sendMessage(colorize("&e/rank list -> Lists the existing ranks"));
        sender.sendMessage(colorize("&c[rank_name] must contain all the color codes and look exactly as it shows"));
        sender.sendMessage(colorize("&e/rank give [username] [rank_name] -> Gives the player the rank"));
        sender.sendMessage(colorize("&e/rank take [username] [rank_name] -> Removes the rank from the player"));
        sender.sendMessage(colorize("&e/rank add [permission] [rank_name] -> Adds the permission to the rank"));
        sender.sendMessage(colorize("&e/rank remove [permission] [rank_name] -> Deletes the permission from the rank"));
        sender.sendMessage(colorize(("&e/rank delete [rank_name] -> Deletes the rank entirely")));
        sender.sendMessage(colorize("&cUse \"&rdefault&c\" as [rank_name] to add/remove permissions from the default rank"));
        sender.sendMessage(colorize("&e/rank setdefault [rank_name] -> Sets the default rank"));
    }

    /**
     * @param username Username of player getting rank updated
     * @param rankName Name of the new rank for the player
     * @return whether a new rank was created or not
     */
    public boolean updateRank(String username, String rankName) {
        try {
            Scanner rankReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/ranks.txt")));
            ArrayList<String> lines = new ArrayList<>();
            while (rankReader.hasNextLine()) {
                lines.add(rankReader.nextLine());
            }
            rankReader.close();

            Scanner defaultReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/defaultRank.txt")));
            String defaultRank = defaultReader.nextLine();
            defaultReader.close();

            String oldRankName = null;
            BufferedWriter rankWriter = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/ranks.txt"));
            for (String line : lines) {
                if (line.contains(username)) {
                    rankWriter.write(username + " " + rankName);
                    oldRankName = line.substring(username.length()+1);
                } else {
                    rankWriter.write(line);
                }
            }
            rankWriter.close();

            Scanner activeRankReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/activeRanks.txt")));
            lines.clear();
            while (activeRankReader.hasNextLine()) {
                lines.add(activeRankReader.nextLine());
            }
            activeRankReader.close();

            BufferedWriter activeRankWriter = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/activeRanks.txt"));
            for (String line : lines) {
                if (line.contains(username)) {
                    activeRankWriter.write(username + " " + rankName);
                } else {
                    activeRankWriter.write(line);
                }
            }
            activeRankWriter.close();

            if (oldRankName == null) { throw new NullPointerException("Unreachable"); }

            if (!oldRankName.equals(defaultRank)) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + username + " removegroup " + oldRankName);
            }

            if (rankName.equals(defaultRank)) {
                return false;
            }

            Scanner existingReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/existingRanks.txt")));
            lines.clear();
            while (existingReader.hasNextLine()) {
                String line = existingReader.nextLine();
                if (line.contains(rankName)) {
                    existingReader.close();
                    return false; }
                lines.add(line);
            }
            existingReader.close();

            BufferedWriter existingWriter = new BufferedWriter(new FileWriter("./plugins/Easy Ranks/existingRanks.txt"));
            for (String line : lines) {
                existingWriter.write(line + "\n");
            }
            existingWriter.write(rankName);
            existingWriter.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean checkExistingRanks(String rankName) {
        try {
            Scanner existingReader = new Scanner(new BufferedReader(new FileReader("./plugins/Easy Ranks/existingRanks.txt")));
            while (existingReader.hasNextLine()) {
                if (existingReader.nextLine().equals(rankName)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


}