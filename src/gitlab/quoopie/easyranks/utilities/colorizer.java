package gitlab.quoopie.easyranks.utilities;

import org.bukkit.ChatColor;

public class colorizer {

    public static String colorize(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

}
